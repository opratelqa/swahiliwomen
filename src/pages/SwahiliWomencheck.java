package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class SwahiliWomencheck extends TestBaseTG {
	
	final WebDriver driver;
	public SwahiliWomencheck(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("back_loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-32']/a")
	private WebElement titulo;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-46']/a")
	private WebElement titulo2;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-45']/a")
	private WebElement titulo3;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-44']/a")
	private WebElement titulo4;
	
	@FindBy(how = How.XPATH,using = "//*[@id='menu-item-43']/a")
	private WebElement titulo5;
	
	
	//*****************************

	
	public void logInSwahiliWomen(String apuntaA) {
		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Swahili Women - LogIn");
		
	cargando(500);
	espera(500);
	WebElement inicio = driver.findElement(By.xpath("//a[contains(text(), 'fashion & beauty')]"));
	inicio.click();
	
	cargando(500);
	espera(9000);
	
	WebElement historias = driver.findElement(By.xpath("//a[contains(text(), 'be a mom')]"));
	historias.click();
	
	cargando(500);
	espera(900);
	
	WebElement blog = driver.findElement(By.xpath("//a[contains(text(), 'healthy life')]"));
	blog.click();
	
	cargando(500);
	espera(900);
	
	WebElement Paseame = driver.findElement(By.xpath("//a[contains(text(), 'pleasure')]"));
	Paseame.click();
	
	cargando(500);
	espera(900);
	
	WebElement Adoptame = driver.findElement(By.xpath("//a[contains(text(), 'confidence')]"));
	Adoptame.click();
	
	cargando(500);
	espera(900);
	
	//valida que los menus se vean correctamente
	

		WebElement contacto = driver.findElement(By.xpath("//a[contains(text(), 'contact us')]"));
		//contacto.click();
		
		espera(3000);
		
		WebElement terminos = driver.findElement(By.xpath("//a[contains(text(), 'terms and conditions')]"));
		//terminos.click();
		
		
		espera(900);
		
		
	System.out.println("Resultado Esperado:Deberan visualizarce las secciones de SwahiliWomen"); 
	System.out.println();
	System.out.println("Fin de Test SwahiliWomen - Log In");
		

	}		

}  

